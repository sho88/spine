const IndexController = () => {
	function indexAction(req, res) {
		res.send({
			message: 'eLearning API'
		});
	}

	return {
		indexAction
	}
};

module.exports = IndexController;