const EnrolController = require('./EnrolController')
	, HistoryController = require('./HistoryController')
	, IndexController = require('./IndexController')
	, CourseController = require( './CourseController' )
	, TopicController = require( './TopicController' )
	, UserController = require('./UserController')
	, UserTypeController = require('./UserTypeController');

module.exports = {
	EnrolController,
	HistoryController,
	IndexController,
	CourseController,
	TopicController,
	UserController,
	UserTypeController
};
