const Services = require('./../services');

const UserTypeController = {
  indexAction (req, res) {
    Services.UserTypeService.getAll().
      then(usertypes => res.send(usertypes)).
      catch(err => res.send(err));
  },
  createAction (req, res) {
    Services.UserTypeService.save(req.body).
      then(usertype => res.send(usertype)).
      catch(err => res.send(err));    
  },
  viewAction (req, res) {
    res.send(req.usertype);
  },
  editAction (req, res) {
    Services.UserTypeService.save({ ...req.usertype.toJSON(), ...req.body }).
      then(usertype => res.send(usertype)).
      catch(err => res.send(err));
  },
  deleteAction (req, res) {
    Services.UserTypeService.delete(req.params.id).
      then(deleted => res.send({ deleted: deleted ? true : false })).
      catch(err => res.send(err));
  }
};

module.exports = UserTypeController;
