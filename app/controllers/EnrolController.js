const Services = require('./../services');

const EnrolController = {
  indexAction (req, res) {
    Services.EnrolService.getAll().
      then(enrols => res.send(enrols)).
      catch(err => res.send(err));
  },
  createAction (req, res) {
    Services.EnrolService.save(req.body).
      then(enrol => res.send(enrol)).
      catch(err => res.send(err));    
  },
  viewAction (req, res) {
    res.send(req.enrol);
  },
  editAction (req, res) {
    Services.EnrolService.save({ ...req.enrol.toJSON(), ...req.body }).
      then(enrol => res.send(enrol)).
      catch(err => res.send(err));
  },
  deleteAction (req, res) {
    Services.EnrolService.delete(req.params.id).
      then(deleted => res.send({ deleted: deleted ? true : false })).
      catch(err => res.send(err));
  }
};

module.exports = EnrolController;
