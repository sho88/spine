const R = require('ramda');
const TopicService = require("./../services/TopicService");

const TopicController =  {
  indexAction (req, res) {
    TopicService.getAll().
      then(topics => res.send(topics)).
      catch(err => res.send(err));
  },
  
  createAction (req, res) {
    TopicService.save(req.body).
      then(topic => res.send(topic)).
      catch(err => res.send(err));
  },

  deleteAction (req, res) {
    TopicService.delete(req.params.id).
      then(deleted => res.send({ deleted: deleted ? true : false })).
      catch(err => res.send(err));
  },
  
  editAction (req, res) {
    const topic = R.omit(['course'], req.topic.toJSON());
    TopicService.save({ ...topic, ...req.body }).
      then(topic => res.send(topic)).
      catch(err => res.send(err));    
  },

  viewAction (req, res) { 
    res.send(req.topic);
  }
};

module.exports = TopicController;
