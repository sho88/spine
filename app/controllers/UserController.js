const HashService = require("./../services/HashService");
const UserService = require("./../services/UserService");
const R = require("ramda");

const UserController = {
  indexAction (req, res) {
    UserService.getAll().
      then(users => res.send(users)).
      catch(err => res.send(err));
  },

  createAction (req, res) {
    HashService.
      generate(req.body.password).
      then(password => {
        const body = {
          ...req.body,
          id: undefined,
          password,
          created_at: Date.now(),
          updated_at: Date.now()
        };
        UserService.save(body).
          then(([id]) => UserService.getOne(id)).
          then(user => res.send(R.head(user))).
          catch(err => res.send(err));
      });
  },

  viewAction (req, res) {
    res.send(req.user);
  },

  editAction (req, res) {
    UserService.save({ ...req.user.toJSON(), ...req.body }).
      then(user => res.send(user)).
      catch(err => res.send(err));
  },

  deleteAction (req, res) {
		UserService.delete(req.params.id).
			then(deleted => res.send({ deleted: deleted ? true : false })).
			catch(err => res.send(err));
  }
};

module.exports = UserController;
