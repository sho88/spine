const process = require('process');
const jwt = require('jsonwebtoken');

function LoginController({ userModel }) {
	function authorizeAction(req, res) {
		const options = {
			attributes: ['id', 'email_address', 'usertype_id'],
			where: {
				email_address: req.body.email
			}
		}

		userModel.
			findOne(options).
			then(user => {
				// send unauthorized statusCode
				if (!user) res.sendStatus(401)

				jwt.sign({ _id: user.id, role: user.usertype_id }, process.env.JWT_KEY, { expiresIn: '1800s' }, (error, token) => {
					if (error) return res.status(500).json({ error });
					res.status(200).json({ token, response: 'success' })
				});
			}).
			catch(err => res.status(401).json({
				err: err.toString()
			}));
	}

	function retrieveAction (req, res) {
		return res.json({ message: 'hello world' });
	}

	return { authorizeAction, retrieveAction }
}

module.exports = LoginController;
