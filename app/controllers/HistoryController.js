const { HistoryService } = require('./../services');
const HttpStatusCodes = require('http-status-codes');

const HistoryController = {
  indexAction (req, res) {
    HistoryService.find(req.query).
      then(history => res.send(history)).
      catch(err => res.send(err));
  },

  createAction (req, res) {
    HistoryService.save(req.body).
      then(history => res.status(HttpStatusCodes.CREATED).send(history)).
      catch(err => res.send(err));
  },

  editAction (req, res) {
    HistoryService.save({ ...req.history.toJSON(), ...req.body }).
      then(history => res.status(HttpStatusCodes.NO_CONTENT).send(history)).
      catch(err => res.send(err));
  },

  deleteAction (req, res) {
    HistoryService.delete(req.params.id).
    then(deleted => res.status(HttpStatusCodes.NO_CONTENT).send({ deleted: deleted ? true : false })).
    catch(err => res.send(err));
  }
}

module.exports = HistoryController;
