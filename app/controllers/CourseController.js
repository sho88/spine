const CourseService = require("./../services/CourseService");
const httpStatusCodes = require("http-status-codes");

const CourseController = {
  indexAction (req, res) {
    CourseService.getAll().
      then(courses => res.status(httpStatusCodes.OK).send(courses)).
      catch(err => res.send(err));
  },

  createAction (req, res) {
    CourseService.save({
      ...req.body,
      created_at: Date.now(),
      updated_at: Date.now()
    }).
      then(model => res.send(model)).
      catch(err => res.send(err));
  },

  viewAction (req, res) {
    res.send(req.course);
  },

  editAction (req, res) {
    CourseService.save({ ...req.course.toJSON(), ...req.body }).
      then(() => CourseService.getOne(req.params.id)).
      then(course => res.send(course)).
      catch(err => res.send(err));
  },

  deleteAction (req, res) {
    CourseService.delete(req.params.id).
      then(deleted => res.send({ deleted: deleted ? true : false })).
      catch(err => res.send({ err: JSON.stringify(err) }));
  },

  topicsAction (req, res) {
    CourseService.getOne(req.params.id).
      then(course => res.send(course.toJSON().topics)).
      catch(err => res.send(err));
  }
};

module.exports = CourseController;
