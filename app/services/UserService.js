// const User = require('./../models/User');
// const R = require("ramda");

// const UserService = {
//   getAll () {
//     return User.fetchAll({ withRelated: ['usertype'] });
//   },
//   getOne (id) {
//     return User.forge({ id }).fetch();
//   },
//   save (body) {
//     const { id } = body;
//     const model = R.omit(['password', 'id'], body);
//     return !id
//       ? User.forge(body).save()
//       : User.forge({ id }).save({ ...model, updated_at: Date.now() });
//   },
//   delete (id) {
//     return User.forge({ id }).destroy();
//   }
// };

const {Service} = require('feathers-sequelize')
const HashService = require('../services/HashService');

exports.UserService = class UserService extends Service {
 
  create (data, params) {
    
    const {photo, first_name, surname, email_address, password, church, telephone_number, usertype_id} = data;
    var hashedPassword = HashService.generate(password);
    
    const userData = {
      photo,
      first_name,
      surname, 
      email_address,
      hashedPassword,
      church,
      telephone_number,
      usertype_id
    }

    return super.create(userData, params);
  }
 
};
