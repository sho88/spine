const bcrypt = require("bcrypt");
const saltRounds = 10;

const HashService = {
  generate (string) {
    return new Promise(resolve => {
      bcrypt.genSalt(saltRounds, (err, salt) => {
        bcrypt.hash(string, salt, (err, result) => resolve(result));
      });
    });
  },

  comparePasswords (password) {
    return new Promise(resolve => {
      bcrypt.compare(password, (err, result) => resolve(result));
    })
  }
};

module.exports = HashService;
