const CourseService = require("./CourseService");
const EnrolService = require("./EnrolService");
const HashService = require("./HashService");
const HistoryService = require("./HistoryService");
const TopicService = require("./TopicService");
const UserService = require("./UserService");
const UserTypeService = require("./UserTypeService");

module.exports = {
  CourseService,
  EnrolService,
  HashService,
  HistoryService,
  TopicService,
  UserService,
  UserTypeService
};
