const Course = require("./../models/Course");
const R = require("ramda");

const CourseService = {
  getAll () {
    return Course.fetchAll({ withRelated: ['topics'] });
  },

  getOne (id) {
    return Course.forge({ id }).fetch({ withRelated: ['topics'] });
  },

  save (body) {
    const { id } = body;
    body.updated_at = id ? Date.now() : body.updated_at;
    const model = R.omit(["id"], body);

    return !id
      ? Course.forge(body).save()
      : Course.forge({ id }).save(model);
  },
  
  delete (id) {
    return Course.forge({ id }).destroy();
  }
};

module.exports = CourseService;
