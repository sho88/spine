const History = require('./../models/History');
const R = require('ramda');

const HistoryService = {
  delete (id) {
    return History.
      forge({ id }).
      delete();
  },

  find (parameters = {}) {
    return History.
      where(parameters).
      fetchAll();
  },

  findOne (id) {
    return History.
      forge({ id }).
      fetch()
  },

  save (body) {
    const { id } = body;
    const model = R.omit(["id"], body);

    return !id
      ? History.forge(body).save()
      : History.forge({ id }).save(model);
  }
};

module.exports = HistoryService;
