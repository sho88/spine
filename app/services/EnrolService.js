const Enrol = require("./../models/Enrol");
const R = require("ramda");

const EnrolService = {
  getAll () {
    return Enrol.fetchAll();
  },
  getOne (id) {
    return Enrol.forge({ id }).fetch();
  },
  save (body) {
    const { id } = body;
    body.updated_at = id ? Date.now() : body.updated_at;
    const model = R.omit(["id"], body);

    return !id
      ? Enrol.forge(body).save()
      : Enrol.forge({ id }).save(model);
  },
  delete (id) {
    return Enrol.forge({ id }).destroy();
  }
};

module.exports = EnrolService;
