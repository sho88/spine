const R = require('ramda');
const UserType = require('./../models/UserType');

const UserTypeService = {
  getAll () {
    return UserType.fetchAll({ withRelated: ['users'] });
  },
  getOne (id) {
    return UserType.forge({ id }).fetch();
  },
  save (body) {
    const { id } = body;
    const model = R.omit(['id'], body);
    return !id
      ? UserType.forge(body).save()
      : UserType.forge({ id }).save({ ...model });    
  },
  delete (id) {
    return UserType.forge({ id }).destroy();
  }
};

module.exports = UserTypeService;
