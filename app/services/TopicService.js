const Topic = require('./../models/Topic');
const R = require('ramda');

const TopicService = {
  delete (id) {
    return Topic.forge({ id }).destroy();
  },

  getAll () {
    return Topic.fetchAll({ withRelated: ['course'] });
  },
  getOne (id) {
    return Topic.forge({ id }).fetch({ withRelated: ['course'] });
  },

  save (body) {
    const { id } = body;
    body.updated_at = id ? Date.now() : body.updated_at;
    const model = R.omit(["id"], body);

    return !id
      ? Topic.forge(body).save()
      : Topic.forge({ id }).save(model);
  },
};

module.exports = TopicService;
