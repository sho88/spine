const model = require('../../db');
const Sequelize = require('sequelize');
const Question = require('./Question');

const Answer = model.define('answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    status: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    question_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: Question,
            key: 'id'
        }
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: false
    }
},
{
    freezeTableName: true
});

module.exports = Answer;