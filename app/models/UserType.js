const model = require('../../db')
const Sequelize = require('sequelize');

const UserType = model.define('UserType', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },

    user_type: {
        type: Sequelize.STRING,
        allowNull: false,
    }
});

module.exports = UserType;