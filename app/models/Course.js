const model = require('../../db')
const Sequelize = require('sequelize');


const Course = model.define('course', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    course_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.STRING,
        allowNull: false
    },
    duration: {
        type: Sequelize.STRING,
        allowNull: false
    }
},
{
    freezeTableName: true,
});

module.exports = Course;
