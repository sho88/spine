const model = require('../../db');
const Sequelize = require('sequelize');
const User = require('./User');
const Course = require('./Course');
const Topic = require('./Topic');

const History = model.define('watchhistory', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    user_id: {
        type: Sequelize.INTEGER,
        references: {
            model: User,
            key: 'id'
        }
    },
    course_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Course,
            key: 'id'
        }
    },
    topic_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Topic,
            key: 'id'
        }
    },
    completed: {
        type: Sequelize.INTEGER,
    },
},
{
  freezeTableName: true,
  timestamps: false
});

module.exports = History;