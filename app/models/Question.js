const model = require('../../db');
const Sequelize = require('sequelize');
const Topic = require('./Topic');

const Question = model.define('Question', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    topic_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: Topic,
            key: 'id'
        }
    },
    content: {
        type: Sequelize.STRING,
        allowNull: false
    }
},
{
    freezeTableName: true
});

module.exports = Question;