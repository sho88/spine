const Answer = require('../models/Answer'),
    Course = require('../models/Course'),
    Enrol = require('../models/Enrol'),
    Group = require('../models/Group'),
    History = require('../models/History'),
    Question = require('../models/Question'),
    Topic = require('../models/Topic'),
    User = require('../models/User'),
    UserGroup = require('../models/UserGroup'),
    UserType = require('../models/UserType')

module.exports = {
    Answer,
    Course,
    Enrol,
    Group,
    History,
    Question,
    Topic,
    User,
    UserGroup,
    UserType
}