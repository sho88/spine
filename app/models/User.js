const model = require('../../db')
const Sequelize = require('sequelize');
const UserType = require('./UserType');

const Users = model.define('users', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    photo: {
        type: Sequelize.STRING,
        alowNull: true
    },
    first_name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    surname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email_address: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {     
        type: Sequelize.STRING,
        allowNull: false
    },
    church: {
        type: Sequelize.STRING,
        allowNull: true
    },
    telephone_number: {
        type: Sequelize.STRING,
        allowNull: true
    },
    usertype_id: {
        type: Sequelize.INTEGER,
        references: {
            model: UserType,
            key: 'id'
        }
    }
}, {
    freezeTableName: true,
})

module.exports = Users;