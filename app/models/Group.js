const model = require('../../db');
const Sequelize = require('sequelize');

const Group = model.define('groups', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  }
},
{
  freezeTableName: true,
  timestamps: false
});

module.exports = Group;