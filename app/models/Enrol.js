const model = require('../../db')
const Sequelize = require('sequelize');
const Users = require('./User');
const Course = require('./Course');

const Enrol = model.define('courseenrol', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    user_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Users,
            key: 'id'
        }
    },
    course_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Course,
            key: 'id'
        }
    }
},
{
    freezeTableName: true,
})

module.exports = Enrol;