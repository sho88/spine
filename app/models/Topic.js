const model = require('../../db')
const Sequelize = require('sequelize');
const Course = require('./Course')

const Topic = model.define('topic', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    topic_name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false,
    },
    content: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    course_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Course,
            key: 'id'
        }
    },
    video: {
        type: Sequelize.STRING,
        allowNull: false
    }
},
{
  freezeTableName: true,
});

module.exports = Topic;
