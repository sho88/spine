const model = require('../../db');
const Sequelize = require('sequelize');
const Group = require('../models/Group');
const User = require('../models/User')

const UserGroup = model.define('UserGroup', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: User,
            key: 'id'
        }
    },
    gropus_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: Group,
            key: 'id'
        }
    }
},
{
    freezeTableName: true,
    timestamps: false
})

module.exports = UserGroup;