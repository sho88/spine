module.exports = ({ hashService }) => ({
  before: {
    create: ctx => new Promise(resolve => {
      hashService.
        generate(ctx.data.password).
        then(password => resolve({ ...ctx, data: { ...ctx.data, password }})).
        catch(err => console.log(err)); // come back to find a way to handle this...
    })
  }
});
