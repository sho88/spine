/* eslint-disable complexity */
const dotenv = require('dotenv');
const process = require('process');
const {Op} = require('sequelize');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const JWTStrategy = require('passport-jwt').Strategy;
const userModel = require('../models/User');

const config = dotenv.config()
const HashService = require('../services/HashService');
// eslint-disable-next-line no-unused-vars
const { req, res, next} = require('express');
const httpStatus = require('http-status-codes');

if (config.error) throw config.error


passport.use("login", new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},function (email, password, done) {
  userModel.findOne({
    where: {
      email_address: email
    }
  }).then(user => {  
    if (user === null){
      var message = email + ' does not exist. Please create an account first before trying to login';
      return done({msg: message});
    }
    const validPassword = HashService.comparePasswords(password);
    if (!validPassword) return done(null, false, {error: 'Your password is invalid'});

    return done(null, user);
  }).catch(error => {
    return done(error);
  });
}));

const Login = (req, res, next) => {
  passport.authenticate( "login", { session: false }, (err, user, info) => {
    console.log('Error is', err);
    console.log('User is', user.dataValues);
    console.log('Info is', info);
    // if an error is returned, return the error as json to the user
    if (err) return res.json({ errors: err });
    if (info) return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({ error: info });

    req.body.user_id = user.dataValues.id;
    req.body.usertype_id = user.dataValues.usertype_id;
    return next();
  })(req, res, next);
};

passport.use("admin-auth", new JWTStrategy({
  secretOrKey: process.env.JWT_KEY,
  jwtFromRequest: req => req.cookies.jwt
}, async function (payload, done) {

   const user = await userModel.findOne({
     where: {
    [Op.and]: [
      {email_address: payload.email},
      {usertype_id: 1}
    ]
    }
  });

  try {
    //no user was found with the email and usertype combination hence return unauthorised
    if (user === null) return done("unauthorised", false, "You are not authorised to view this portion of the website");
    else return done(null, true, "authorised");
  } catch (error){
    return done(error, false, "an error occurred during auth check");
  }
}));

const authenticateAdmin = (req, res, next) => {
   console.log(req.cookies)
   if (req.method !== 'POST' && req.method && 'PUT' && req.method !== 'DELETE')
   return next()
    passport.authenticate("admin-auth",{ session: false }, (err, admin, info) => {
      //info comes up as undefined even though there is a string passed
      //I can't figure out why

      //if (err !== "unauthorised") return res.json({ errors: err, message: info.message });

        if (!admin)
        return res.status(httpStatus.FORBIDDEN).send({
          error: err,
          message: info
        });

        return next();
      })(req, res, next);
}


module.exports = { AuthenticateAdmin: authenticateAdmin, Login }
