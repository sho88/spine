const process = require('process');
const environment = process.env.NODE_ENV || 'development'
const jwt = require('jsonwebtoken');
//-------------------------------------------------------------------------------------------------
// environment middleware
function environmentMiddleware (req, res, next) {
  console.log(`Currently on the ${environment} environment.`);
  if (environment === "local" || environment === "development"){
    //res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Auth-Dev, CSRF-Token");
    res.set("Access-Control-Allow-Credentials", "true");
  }
  next();
}

// protect routes that need to have authorized
function protect (req, res, next) {
  if (req.headers && !req.headers['authorization']) return res.sendStatus(401);

  const [, token] = req.headers['authorization'].split(' ');
  jwt.verify(token, process.env.JWT_KEY, (err, user) => {

  if (err)
    return res.sendStatus(403)

    req.user = user;
    next();
  });
}

// responsible for checking if this is an admin or not
function adminProtect (req, res, next) {
  console.log('I am here now!');
  if (req.method === 'POST' && !_hasCreationPermission(req)) return res.sendStatus(403);
  return next();
}

// responsible for checking if the user is an admin or lecturer
function _hasCreationPermission (req) {
  const ADMIN = 1;
  const LECTURER = 2;

  return (req.user.role !== ADMIN || req.user.role !== LECTURER);
}
//-------------------------------------------------------------------------------------------------
module.exports = {
  adminProtect,
  environmentMiddleware,
  protect,
};
