const { check } = require("express-validator");

const UserMiddleware = {
  checkId: [check("id").isNumeric()],
  checkUserBody: [check("").exists()]
};

module.exports = UserMiddleware;
