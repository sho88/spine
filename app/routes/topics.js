const express = require('express');
const R = require("ramda");
const httpStatusCodes = require('http-status-codes');
const Controllers = require('./../controllers');
const TopicService = require("./../services/TopicService");

module.exports = express.Router().
  param('id', (req, res, next) => {
    TopicService.getOne(req.params.id).
      then(topic => {
        if (R.isEmpty(topic)) throw { message: "Topic cannot be found." };

        req.topic = topic;
        next();
      }).
      catch(err => res.status(httpStatusCodes.NOT_FOUND).send(err));
  }).
  get('/', Controllers.TopicController.indexAction).
  post('/', Controllers.TopicController.createAction).
  get('/:id', Controllers.TopicController.viewAction).
  put('/:id', Controllers.TopicController.editAction).
  delete('/:id', Controllers.TopicController.deleteAction);
