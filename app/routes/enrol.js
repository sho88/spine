const express = require("express");
const Controllers = require("./../controllers");
const Services = require("./../services");

module.exports = express.
  Router().
  param("id", (req, res, next) => {
    Services.EnrolService.
      getOne(req.params.id).
      then(enrol => {
        req.enrol = enrol;
        next();
      }).
      catch(err => next(err));
  }).

  // get all the resources
  get("/", Controllers.EnrolController.indexAction).

  // create a single resource
  post("/", Controllers.EnrolController.createAction).

  // get a single resource
  get("/:id", Controllers.EnrolController.viewAction).

  // update a single resource
  put("/:id", Controllers.EnrolController.editAction).

  // delete a single resource
  delete("/:id", Controllers.EnrolController.deleteAction);
