const express = require('express');
const LoginController = require('./../controllers/LoginController');

module.exports = ({ userModel }) => {
	const router = express.Router();
	const controller = LoginController({ userModel });

	router.post('/', controller.authorizeAction);
	router.get('/retrieve', controller.retrieveAction);

	return router;
};
