const express = require("express");
const R = require("ramda");
const httpStatusCodes = require("http-status-codes");
const Controllers = require("./../controllers");
const UserService = require("./../services/UserService");

module.exports = express.Router().
  // ensure that the ID parameter loads a user with this specific ID
  param("id", (req, res, next) => {
    UserService.getOne(req.params.id).
      then(user => {
        if (R.isEmpty(user)) throw { message: "User cannot be found." };

        req.user = user;
        next();
      }).
      catch(err => res.status(httpStatusCodes.NOT_FOUND).send(err));
  }).
  // view multiple resources
  get("/", Controllers.UserController.indexAction).
  // create a new resource
  post("/", Controllers.UserController.createAction).
  // get a single resource
  get("/:id", Controllers.UserController.viewAction).
  // edit a single resource
  put("/:id", Controllers.UserController.editAction).
  // delete resource(s)
  delete("/:id", Controllers.UserController.deleteAction);
