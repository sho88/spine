const express = require('express');
const R = require("ramda");
const httpStatusCodes = require('http-status-codes');
const Controllers = require('./../controllers');
const Services = require('./../services');

module.exports = express.Router().
  param('id', (req, res, next) => {
    Services.UserTypeService.getOne(req.params.id).
      then(usertype => {
        if (R.isEmpty(usertype)) throw { message: "Usertype cannot be found." };
        req.usertype = usertype;
        next();
      }).
      catch(err => res.status(httpStatusCodes.NOT_FOUND).send(err));
  }).
  get('/', Controllers.UserTypeController.indexAction).
  post('/', Controllers.UserTypeController.createAction).
  get('/:id', Controllers.UserTypeController.viewAction).
  put('/:id', Controllers.UserTypeController.editAction).
  delete('/:id', Controllers.UserTypeController.deleteAction);