const express = require("express");
const Controllers = require("./../controllers");
const CourseService = require("./../services/CourseService");

module.exports = express.
  Router().
  param("id", (req, res, next) => {
    CourseService.getOne(req.params.id).
      then(course => {
        req.course = course;
        next();
      }).
      catch(err => next(err));
  }).

  // get all the resources
  get("/", Controllers.CourseController.indexAction).

  // create a single resource
  post("/", Controllers.CourseController.createAction).

  // get a single resource
  get("/:id", Controllers.CourseController.viewAction).

  // get resources associated to this specific resource
  get("/:id/topics", Controllers.CourseController.topicsAction).

  // update a single resource
  put("/:id", Controllers.CourseController.editAction).

  // delete a single resource
  delete("/:id", Controllers.CourseController.deleteAction);
