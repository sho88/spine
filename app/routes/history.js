const express = require('express');
const { HistoryController } = require('./../controllers');
const { HistoryService } = require('./../services');

module.exports = express.Router().
  // if we want to get the history by user ID
  param('id', (req, res, next) => {
    HistoryService.
      findOne(req.params.id).
      then(history => {
        req.history = history;
        next();
      }).
      catch(err => next(err));
  }).
  get('/', HistoryController.indexAction).
  post('/', HistoryController.createAction).
  put('/:id', HistoryController.editAction).
  delete('/:id', HistoryController.deleteAction);
