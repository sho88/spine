const {
  AuthenticationService,
} = require("@feathersjs/authentication");
const { LocalStrategy } = require("@feathersjs/authentication-local");

class MyLocalStrategy extends LocalStrategy {
  async getEntityQuery (query) {
    // Query for user but only include users marked as `active`
    return {
      ...query,
      active: true,
      $limit: 1,
    };
  }
}

module.exports = (app) => {
  const authService = new AuthenticationService(app);

  authService.register("local", new MyLocalStrategy());

  // ...
  app.use("/authentication", authService);
};
