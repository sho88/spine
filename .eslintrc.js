module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "es6": true
    },
    "extends": [
        "eslint:recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018
    },
    "rules": {
        "brace-style": "error",
        "complexity": ["error", 3],
        "curly": ["error", "multi"],
        "dot-location": ["error", "object"],
        "eqeqeq": ["error", "always"],
        "no-alert": "error",
        "no-unused-vars": "error",
        "space-before-blocks": ["error", {
            "functions": "always",
            "keywords": "never",
            "classes": "always"
        }],
    }
};