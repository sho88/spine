-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for elearning
CREATE DATABASE IF NOT EXISTS `elearning` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `elearning`;

-- Dumping structure for table elearning.answer
CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) unsigned DEFAULT NULL,
  `question_id` int(11) unsigned DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_answer_question` (`question_id`),
  CONSTRAINT `c_fk_answer_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.answer: ~12 rows (approximately)
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` (`id`, `status`, `question_id`, `content`, `createdAt`, `updatedAt`) VALUES
	(1, 0, 1, 'You do not need to accept Christ before you can be baptised with the Holy Spirit', '0000-00-00 00:00:00', 0),
	(2, 0, 1, 'John the baptised was baptised with the Holy Spirit', '0000-00-00 00:00:00', 0),
	(3, 1, 1, 'John 3:16 talks about the love of God for his people by giving his only son.', '0000-00-00 00:00:00', 0),
	(4, 0, 1, 'Nebucadnezzar was the second king of Judah', '0000-00-00 00:00:00', 0),
	(5, 0, 2, 'Adoration Confusion Thoughtfulness Supplication', '0000-00-00 00:00:00', 0),
	(6, 1, 2, 'Adoration Confession Prayer Supplication', '0000-00-00 00:00:00', 0),
	(7, 0, 2, 'Affection Concession Thanksgiving Supplication', '0000-00-00 00:00:00', 0),
	(8, 0, 2, 'Application Confession Thanksgiving Supplication', '0000-00-00 00:00:00', 0),
	(9, 0, 3, 'Adam\'s faith in Woman', '0000-00-00 00:00:00', 0),
	(10, 0, 3, 'Eve\'s belief in the serpent\'s word', '0000-00-00 00:00:00', 0),
	(11, 0, 3, 'Lust of the flesh, eyes and pride of life', '0000-00-00 00:00:00', 0),
	(12, 1, 3, 'Man\'s disobedience towards GOD', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;

-- Dumping structure for table elearning.course
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.course: ~4 rows (approximately)
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`, `course_name`, `description`, `duration`, `createdAt`, `updatedAt`) VALUES
	(1, 'CORE Series I', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '6 Week duration', '0000-00-00 00:00:00', 2017),
	(2, 'CORE Series II', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '12 Week duration', '0000-00-00 00:00:00', 2017),
	(5, '12 M\'s of Ministry', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '1 Week duration', '0000-00-00 00:00:00', 2017),
	(38, 'Spiritual Beings', 'In the first pages of the Bible, we’re introduced to God and humans as the main characters. But there’s also a wspinee cast of spiritual beings who play an important role throughout the Bible, though they’re often in the background. In this video, we begin to explore these beings and how they fit into the unified storyline of the Bible.', '4 weeks', '0000-00-00 00:00:00', 4294967295),
	(54, 'Biblical Themes', 'This fully animated series traces key biblical themes from their first appearance through the entire narrative of the Bible.', '21 days', '2020-06-27 09:01:38', 2020),
	(55, 'Reading Moshe, Seeing Yeshua Ha\'Mashiach', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2 weeks', '2020-11-14 10:53:10', 2020);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- Dumping structure for table elearning.courseenrol
CREATE TABLE IF NOT EXISTS `courseenrol` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `course_id` int(11) unsigned DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_courseenroll_user` (`user_id`),
  KEY `index_foreignkey_courseenroll_course` (`course_id`),
  CONSTRAINT `c_fk_courseenroll_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `c_fk_courseenroll_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.courseenrol: ~3 rows (approximately)
/*!40000 ALTER TABLE `courseenrol` DISABLE KEYS */;
INSERT INTO `courseenrol` (`id`, `user_id`, `course_id`, `createdAt`, `updatedAt`) VALUES
	(4, 14, 1, '2020-10-15 08:54:32', 2020),
	(5, 14, 38, '2020-11-04 18:54:32', 2020),
	(6, 14, 54, '2020-11-04 18:54:34', 2020);
/*!40000 ALTER TABLE `courseenrol` ENABLE KEYS */;

-- Dumping structure for table elearning.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table elearning.groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Dumping structure for table elearning.invitaton
CREATE TABLE IF NOT EXISTS `invitaton` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invite_from` int(11) NOT NULL,
  `invite_to` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table elearning.invitaton: ~0 rows (approximately)
/*!40000 ALTER TABLE `invitaton` DISABLE KEYS */;
/*!40000 ALTER TABLE `invitaton` ENABLE KEYS */;

-- Dumping structure for table elearning.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table elearning.migrations: ~14 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `timestamp`, `name`) VALUES
	(12, 1550269635879, 'UserTableMigration1550269635879'),
	(13, 1550585884211, 'ExamTableMigration1550585884211'),
	(14, 1550591708434, 'QuestionTableMigration1550591708434'),
	(15, 1550676102727, 'AnswerTableMigration1550676102727'),
	(16, 1550678241661, 'TermTableMigration1550678241661'),
	(17, 1550748786578, 'TermCourseMigration1550748786578'),
	(18, 1551869976432, 'CourseTableMigration1551869976432'),
	(20, 1552170268527, 'InvitationTableMigration1552170268527'),
	(21, 1552171478313, 'GroupTableMigration1552171478313'),
	(22, 1552172137961, 'GroupUserTableMigration1552172137961'),
	(23, 1552306271988, 'EvaluationQuestionTable1552306271988'),
	(24, 1552309933298, 'EvaluationAnswerTableMigration1552309933298'),
	(25, 1551870834714, 'TopicTableMigration1551870834714'),
	(28, 1555692575132, 'VideosWatchedTableMigration1555692575132');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table elearning.question
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) unsigned DEFAULT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.question: ~3 rows (approximately)
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` (`id`, `topic_id`, `content`, `createdAt`, `updatedAt`) VALUES
	(1, 1, 'Which of the following statements is true?', NULL, NULL),
	(2, 1, 'ACTS is an acryonym used for prayer. What does it stand for?', NULL, NULL),
	(3, 1, 'What caused the fall of man?', NULL, NULL);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;

-- Dumping structure for table elearning.topic
CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `course_id` int(11) unsigned DEFAULT NULL,
  `video` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_topic_course` (`course_id`),
  CONSTRAINT `c_fk_topic_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.topic: ~14 rows (approximately)
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` (`id`, `topic_name`, `description`, `content`, `createdAt`, `updatedAt`, `course_id`, `video`) VALUES
	(1, 'Salvation', 'What we have been saved from, who we have been saved from, and what we have been saved into. What is salvation? Who is the saviour?', 'osdijfdoinhfdsj fdsfondskj dkfds bfkibfdkidfbdsbf dskhf fkdsjbfdskjfbdskjf dskjf dskfbdsfkbfskdjbfgfdskjbgfjbgfkbgdkfjgdnmf gdfkjbgdfk gfdkjgdkf gfdk gdkfj gdkfbgfdkjbgfdkj hgfdnm hgfdkjbgfdk', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1Xt9LNTOmY0'),
	(2, 'Prayer', 'Why do we need to pray? What is the purpose of praying?', 'osdijfdoinhfdsj fdsfondskj dkfds bfkibfdkidfbdsbf dskhf fkdsjbfdskjfbdskjf dskjf dskfbdsfkbfskdjbfgfdskjbgfjbgfkbgdkfjgdnmf gdfkjbgdfk gfdkjgdkf gfdk gdkfj gdkfbgfdkjbgfdkj hgfdnm hgfdkjbgfdk', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'dXi62ajhJX4'),
	(3, 'Christian Character', 'What is character? What is personality? What is expected of a believer (in their conduct) etc', 'osdijfdoinhfdsj fdsfondskj dkfds bfkibfdkidfbdsbf dskhf fkdsjbfdskjfbdskjf dskjf dskfbdsfkbfskdjbfgfdskjbgfjbgfkbgdkfjgdnmf gdfkjbgdfk gfdkjgdkf gfdk gdkfj gdkfbgfdkjbgfdkj hgfdnm hgfdkjbgfdk', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'AsUC_1aTRbI'),
	(4, 'The Presence of God', 'Talks about the presence of God in our lives. ', 'randksflakjsdlfajldkfjalskdjflaksdjkflajsldkfjalksdjflkajsdlfjaklsdjflkjasdlkfjasdjfiaosdfiljawijflaksdjlfkasjdflkjasldkfjlakdfhalwuehfjsdaf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5, 'SiwnmO0Zyq0'),
	(42, 'Tree of Life', 'In the opening pages of the Bible, God gives humanity a gift that they quickly forfeit—eternal life that comes by eating from the tree of life. In this video, we explore the meaning of this powerful image and how sacred trees play a key role throughout the story of the Bible. It all leads up to Jesus himself, who died upon a tree so that he could become a new tree of life for all humanity.', '', '2020-06-27 09:05:44', '2020-06-27 09:05:44', 54, 'TJLan-pJzfQ'),
	(43, 'Generosity', 'In the story of the Bible, God is depicted as a generous host who provides for the needs of his guests. However, humans live from a mindset of scarcity and hoard God’s many gifts. In this video, we explore God’s plan for overcoming our selfishness by giving the ultimate gift of himself in the person of Jesus.', '', '2020-06-27 09:06:19', '2020-06-27 09:06:19', 54, '62CliEkRCso'),
	(44, 'Temple', 'In this video, we explore how Israel’s temple in the Bible is described as the place where God’s space and humanity’s space are one. In fact, the whole biblical drama can be told as a story about God’s temple. In the opening pages of Genesis, God creates a cosmic temple, and in the person of Jesus, God takes up personal residence in his temple-world. By the end of the biblical story, all of creation has become God’s sacred temple.', '', '2020-06-27 09:06:49', '2020-06-27 09:06:49', 54, 'wTnq6I3vUbU'),
	(45, 'God', 'The God portrayed in the Bible isn’t easy to understand, but what if we could better understand what it is that we can’t understand? In this video, we will explore the complex identity of God displayed in the storyline of the Bible, and (surprise!) it all leads to Jesus.', '', '2020-06-27 09:07:30', '2020-06-27 09:07:30', 54, 'eAvYmE2YYIU'),
	(46, 'Angels and Cherubim', 'Did you know that angels in the Bible don’t have wings? Or that cherubim are not cute, chubby babies? In this video, we explore the biblical portrayals of these spiritual beings to understand just who they are and what role they play in the story of the Bible.', '', '2020-06-27 09:08:29', '2020-06-27 09:08:29', 54, '-bMRxQbLUlg'),
	(47, 'Holy Spirit', 'In this video, we explore the original meaning of the biblical concept of “spirit” and what it means that God’s Spirit is personally present in all of creation. Ultimately, the Spirit was revealed through Jesus and sent out into the lives of his followers to bring about the new creation.', '', '2020-06-27 09:09:17', '2020-06-27 09:09:17', 54, 'oNNZO9i1Gjc'),
	(48, 'Messiah', 'In this video, we explore the mysterious promise on page three of the Bible, that a promised deliverer would one day come to confront evil and rescue humanity. We trace this theme through the family of Abraham, the messianic lineage of David, and ultimately to Jesus who defeated evil by letting it defeat him.', '', '2020-06-27 09:10:23', '2020-06-27 09:10:23', 54, '3dEh25pduQ8'),
	(49, 'The Law', 'In this video, we explore the importance of the ancient laws in the Old Testament. Why are they in the Bible, and what do they say to followers of Jesus? We explore how they fulfilled a strategic purpose in one key phase of the biblical story, leading up to Jesus who fulfilled the law and summarized it in the call to love God and love your neighbor as yourself.', '', '2020-06-27 09:11:05', '2020-06-27 09:11:05', 54, '3BGO9Mmd_cU'),
	(50, 'The Way of the Exile', 'If followers of Jesus are to give their total allegiance to God’s Kingdom, how should they relate to the governments and power structures of their own day? In this video, we’ll see how the experience of Daniel and his friends in Babylonian exile offers wisdom for navigating this tension. Following Jesus in the 21st century means learning the way of the exile.', '', '2020-06-27 09:11:35', '2020-06-27 09:11:35', 54, 'XzWpa0gcPyo'),
	(51, 'Exile', 'Exile is one of the core, yet often overlooked, themes underlying the entire Biblical storyline. In this video, we\'ll see how Israel\'s exile to Babylon is a picture of all humanity\'s exile from Eden. As you might guess, Jesus is the one to open the way back home.', '', '2020-06-27 09:12:13', '2020-06-27 09:12:13', 54, 'xSua9_WhQFE');
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;

-- Dumping structure for table elearning.usergroup
CREATE TABLE IF NOT EXISTS `usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `groups_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `groups_id` (`groups_id`),
  CONSTRAINT `UserGroup_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `UserGroup_ibfk_2` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table elearning.usergroup: ~0 rows (approximately)
/*!40000 ALTER TABLE `usergroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `usergroup` ENABLE KEYS */;

-- Dumping structure for table elearning.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `church` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `usertype_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_user_usertype` (`usertype_id`),
  CONSTRAINT `c_fk_user_usertype_id` FOREIGN KEY (`usertype_id`) REFERENCES `usertype` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `photo`, `first_name`, `surname`, `email_address`, `password`, `church`, `telephone_number`, `createdAt`, `updatedAt`, `usertype_id`) VALUES
	(1, '123.jpg', 'Jake', 'White', 'jake.white@gmail.com', '$2b$10$4jDdO2iwxagVVJ2ZBkP.Ku79Sns5x05es/FvFiRpLW8rns7xE673O', 'Hillsong London', '07987342567', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
	(2, NULL, 'Ekow', 'Mensah', 'yamoah.ensah@yahoo.com', '$2y$10$HXUZXMLgGvGsfeq49yRHVe.gaYd/vIuua6A6joh.hUiHwE2yuDhqq', 'Temple Church', '07925602856', NULL, NULL, 3),
	(3, NULL, 'Karen', 'Tay', 'karentay@yahoo.com', '$2y$10$mJzBQiU/2fvwo.Cdcp7/1OIvRbf/tyIiKY5sp97h8EbfpPAnky3Ei', 'Hillsong London', '0789765345', NULL, NULL, 3),
	(4, '123.jpg', 'Admin', 'Admin', 'admin@spine.com', 'password', 'Salvation Army', '0898765456', NULL, NULL, 3),
	(14, NULL, 'Sho', 'Carter-Daniel', 'shosilva@hotmail.co.uk', '$2b$10$KvmsfG6PccjVZchBZ5vyZ.FlQZdjQ7D2Xw1rHEi8/eehQckMgwhx.', 'House of Lights', '07582272091', '2020-06-25 20:06:54', '2020-06-25 20:06:54', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table elearning.usertype
CREATE TABLE IF NOT EXISTS `usertype` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table elearning.usertype: ~5 rows (approximately)
/*!40000 ALTER TABLE `usertype` DISABLE KEYS */;
INSERT INTO `usertype` (`id`, `user_type`) VALUES
	(1, 'admin'),
	(2, 'lecturer'),
	(3, 'student'),
	(4, 'course_leader'),
	(5, 'exec-officer');
/*!40000 ALTER TABLE `usertype` ENABLE KEYS */;

-- Dumping structure for table elearning.watchhistory
CREATE TABLE IF NOT EXISTS `watchhistory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `course_id` int(11) unsigned NOT NULL,
  `topic_id` int(11) unsigned NOT NULL,
  `completed` tinyint(1) unsigned NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Dumping data for table elearning.watchhistory: ~1 rows (approximately)
/*!40000 ALTER TABLE `watchhistory` DISABLE KEYS */;
INSERT INTO `watchhistory` (`id`, `user_id`, `course_id`, `topic_id`, `completed`, `createdAt`) VALUES
	(27, 1, 1, 1, 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `watchhistory` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
