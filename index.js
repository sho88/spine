// For RESTful API best practices: https://restfulapi.net/http-methods/
const express = require('@feathersjs/express');
const process = require('process');
const feathers = require('@feathersjs/feathers');
const service = require('feathers-sequelize')
const cors = require('cors')
const helmet = require('helmet');
const app = express(feathers());
const dotenv = require("dotenv");
const result = dotenv.config();
const db = require('./db');
const cookieParser = require('cookie-parser');

if (result.error) throw result.error
db.authenticate().
then(() => console.log('Connection to database was successful')).
catch(err => console.log('unable to connect to the database', err));

const PORT = process.env.PORT || 1337;

const corsConfig = {
  origin: ['http://localhost:3000', 'http://localhost:4200'],
  credentials: true
}

app.use(helmet());
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({
  extended: true
}));
app.use(cors(corsConfig));
app.configure(express.rest());

// get the controllers from here
const Models = require("./app/models");

const {
  adminProtect,
  environmentMiddleware,
  protect,
} = require('./app/middleware/protect');

const {
  HashService,
} = require('./app/services');

// -----------------------------------------------------------------------------
// for authentication
app.use('/auth/login', require('./app/routes/login')({ userModel: Models.User }));

// apply middleware to the '/api' url and start preparing routes
app.use("/api", cors(corsConfig), protect, environmentMiddleware);
app.use("/api/courses", adminProtect, service({ Model: Models.Course }));
app.use("/api/topics", adminProtect, service({ Model: Models.Topic }));
app.use("/api/users", service({ Model: Models.User }));
app.service('/api/users').hooks(require('./app/hooks/users')({ hashService: HashService }));
app.use("/api/user-types", require("./app/routes/user-types"));
app.use("/api/enrol", service({ Model: Models.Enrol }));
app.use("/api/questions", service({ Model: Models.Question }));
app.use("/api/answers", service({ Model: Models.Answer }));
app.use("/api/groups", service({ Model: Models.Group }));
app.use("/api/user-groups", service({ Model: Models.UserGroup }));
app.use("/api/history", service({ Model: Models.History }));

app.get('/something', protect, (req, res) => res.send('Hello world...'));

// start the port
app.listen(PORT, () => console.log(`Application listening on port ${PORT}`));
