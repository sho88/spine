const process = require('process');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD , {
  host: 'localhost',
  dialect: 'mysql',
  logging: console.log
})

console.log('databaseName: ' +  process.env.DB_DATABASE)

// const knex = require("knex")({
//   client: "mysql",
//   connection: {
//     host: process.env.DB_HOST,
//     user: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD,
//     
//   }
// });

module.exports = sequelize;
